--------------------------------------------------------------------------------
--FUNCTIONS: RETORNAN UN VALOR

--------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_sal
(p_id IN employees.employee_id%TYPE)
RETURN NUMBER IS
v_sal employees.salary%TYPE:= 0;
BEGIN
SELECT salary
INTO
v_sal
FROM employees
WHERE employee_id =p_id;
RETURN
v_sal;
END get_sal;
--------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_sal
(p_id IN employees.employee_id%TYPE) RETURN NUMBER IS
v_sal employees.salary%TYPE:= 0;
BEGIN
SELECT salary INTO
v_sal
FROM employees WHERE employee_id = p_id;
RETURN
v_sal;
EXCEPTION
WHEN NO_DATA_FOUND THEN RETURN NULL;
END get_sal;

DECLARE
v_sal employees.salary%type;
BEGIN
v_sal:= get_sal(999);
DBMS_OUTPUT.PUT_LINE(v_sal);
END;

SELECT job_id, get_sal(employee_id) FROM employees;


--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION valid_dept 
(p_dept_no departments.department_id%TYPE)
RETURN BOOLEAN IS v_valid VARCHAR2(1);
BEGIN
SELECT 'x'
INTO v_valid
FROM departments
WHERE department_id = p_dept_no;
RETURN(true);
EXCEPTION
WHEN NO_DATA_FOUND THEN RETURN(false);
WHEN OTHERS THEN NULL;
END;

SELECT job_id, valid_dept(last_name) FROM employees; --no s epuede generarar la funcion en un sql

--SE PUEDE VISUALIZAR EN UN CODIGO PLSQL
BEGIN
    IF valid_dept(1000) THEN
        dbms_output.put_line('TRUE');
    ELSE
        dbms_output.put_line('FALSE');
    END IF;
END;
--------------------------------------------------------------------------------

--FUNCIONES SIN PARAMETROS













--------------------------------------------------------------------------------
--FUNCIONES EN SENTENCIAS SQL

CREATE OR REPLACE FUNCTION tax(
p_value IN NUMBER)
RETURN NUMBER IS
BEGIN
RETURN (
p_value * 0.08);
END tax;


SELECT employee_id, last_name, salary, tax(salary)
FROM employees
WHERE department_id= 50;

--------------------------------------------------------------------------------
--ERRORES DML EN FUNCIONES

CREATE OR REPLACE FUNCTION query_max_sal(p_dept_id NUMBER)
RETURN NUMBER IS
v_num NUMBER;
BEGIN
SELECT
MAX(salary)INTO v_num FROM employees
WHERE department_id = p_dept_id;
RETURN (v_num);
END;

SELECT DISTINCT
query_max_sal(100) FROM employees;

UPDATE employees
SET salary = query_max_sal(100)
WHERE employee_id= 174;

DECLARE
v_max NUMBER;
begin
SELECT  DISTINCT query_max_sal(100) INTO v_max FROM employees; 
UPDATE employees
SET salary = v_max
WHERE employee_id= 174;
END;

 SELECT employee_id, department_id,salary FROM employees
 WHERE employee_id= 174;
 
 
 SELECT text
FROM
USER_SOURCE
WHERE name = 'TAX'
ORDER BY line;
