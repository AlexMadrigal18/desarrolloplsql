--CURSORES, REALIZADO EN USUARIO HR
--CURSOR: Elemento que guarda registros de una tabla
--------------------------------------------------------------------------------
DECLARE
CURSOR cur_depts IS                                 --SE DECLARA EL CURSOR
SELECT department_id, department_name               --SE HACE EL SELECT DE LO QUE SE QUIERE BUESCAR
FROM departments;                           
v_department_id departments.department_id%TYPE;
v_department_name departments.department_name%TYPE;
BEGIN
OPEN cur_depts;                                     --INICA EL CURSOR, SE REALIZA LA CONSULTA
LOOP
FETCH cur_depts 
INTO v_department_id, v_department_name;            --FETCH: BUSCA VALORES DEL CURSOR Y LOS GUARDA EN LAS VARIABLES, UNA FILA A LA VEZ
EXIT WHEN cur_depts%NOTFOUND;                       --CONDICION PARA TERMINAR LOOP
DBMS_OUTPUT.PUT_LINE(v_department_id||' '||v_department_name); --SALIDA DE DATOS EN PANTALLA
END LOOP;
CLOSE cur_depts;                                    --SE CIERRA EL CURSOR
END;

--------------------------------------------------------------------------------
DECLARE
CURSOR cur_emps IS
SELECT employee_id, last_name, salary
FROM employees
WHERE department_id =30;
v_empno employees.employee_id%TYPE;
v_lname employees.last_name%TYPE;
v_sal employees.salary%TYPE;
BEGIN
OPEN cur_emps;
LOOP
FETCH cur_emps
INTO v_empno, v_lname,v_sal;
EXIT WHEN cur_emps%NOTFOUND;
DBMS_OUTPUT.PUT_LINE(v_empno||' '||v_lname); 
END LOOP;
END;

--------------------------------------------------------------------------------
--ROWTYPE

DECLARE
CURSOR cur_emps IS
SELECT* 
FROM employees
WHERE department_id = 30;
v_emp_record cur_emps%ROWTYPE;
BEGIN
OPEN cur_emps;
LOOP
FETCH cur_emps
INTO v_emp_record;
EXIT WHEN cur_emps%NOTFOUND;
DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' - '|| v_emp_record.last_name);
END LOOP;
CLOSE cur_emps;
END;
--------------------------------------------------------------------------------
--SELECCIONANDO LAS COLUMNAS REQUERIDAS
DECLARE
CURSOR cur_emps_dept IS
SELECT
first_name, last_name, department_name
FROM employees e, departments d
WHERE
e.department_id=d.department_id;
v_emp_dept_record cur_emps_dept%ROWTYPE;
BEGIN
OPEN cur_emps_dept;
LOOP
FETCH cur_emps_dept
INTO v_emp_dept_record;
EXIT WHEN cur_emps_dept%NOTFOUND;
DBMS_OUTPUT.PUT_LINE(v_emp_dept_record.first_name|| ' �'||v_emp_dept_record.last_name|| ' �'|| v_emp_dept_record.department_name);
END LOOP;
CLOSE cur_emps_dept;
END;


--------------------------------------------------------------------------------
--CONDICIONES DE SALIDA: ROWCOUNT: CONTADOR QUE SE INCREMENTA AL HACER UN FETCH

DECLARE
CURSOR cur_emps IS
SELECT employee_id, last_name FROM employees;
v_emp_record cur_emps%ROWTYPE;
BEGIN
OPEN
cur_emps;
LOOP
FETCH cur_emps 
INTO v_emp_record;
EXIT WHEN cur_emps%ROWCOUNT > 10 OR cur_emps%NOTFOUND;
DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id|| ' '|| v_emp_record.last_name);
END LOOP;
CLOSE cur_emps;
END;

--------------------------------------------------------------------------------
--ATRIBUTOS DE CURSOR EXPLICITO
--%ISOPEN Boolean Evaluates to TRUEif the cursor is open.
--%NOTFOUND Boolean Evaluates to TRUE if the most recent fetch didnot return a row.
--%FOUND Boolean Evaluates to TRUE if the most recent fetch returneda row; opposite of %NOTFOUND.
--%ROWCOUNT-Number-Evaluates to the total number of rows FETCHed so far.
--------------------------------------------------------------------------------
--NO SE PUEDE UTILIZAR UN ATRIBUTO DE CURSOR EXPLICITO EN UNA SENTENCIA SQL POR EJEMPLO:
--VALUES(v_emp_record.employee_id,cur_emps%ROWCOUNT, v_emp_record.salary);
--PARA PODER HACERLO, SE DECLARA COMO VARIBALE
DECLARE
CURSOR cur_emps IS
...;
v_emp_record emp_cursor%ROWTYPE;
v_count NUMBER;
v_rowcount NUMBER; --SE DECLARA LA VARIBALE PARA USAR EL ATRIBUTO DEL CURSOR
BEGIN
OPEN cur_emps;
LOOP
FETCH cur_emps INTO v_emp_record;
EXIT WHEN cur_emps%NOTFOUND;
v_rowcount := cur_emps%ROWCOUNT;--SE DEFINE LA VARIABLE CON EL ATRIBUTO DEL CURSOR
INSERT INTO top_paid_emps (employee_id, rank, salary)
VALUES (v_emp_record.employee_id, v_rowcount, v_emp_record.salary); -- VARIABLE UTILIZADA EN LA SENTENCIA SQL

--------------------------------------------------------------------------------