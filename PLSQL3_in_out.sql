DECLARE
v_salary NUMBER(6) := 6000;
v_sal_increase VARCHAR2(5) := '1000';
v_total_salary v_salary%TYPE;
BEGIN
v_total_salary := v_salary + v_sal_increase;
DBMS_OUTPUT.PUT_LINE(v_total_salary);
END;

DECLARE
v_father_name VARCHAR2(20):='Patrick';
v_date_of_birth DATE:='20-Abr-1972';
BEGIN
DECLARE
v_child_name VARCHAR2(20):='Mike';
BEGIN
DBMS_OUTPUT.PUT_LINE('Father''sName: '||v_father_name);
DBMS_OUTPUT.PUT_LINE('Date of Birth: '||v_date_of_birth);
DBMS_OUTPUT.PUT_LINE('Child''sName: '||v_child_name);
END;
DBMS_OUTPUT.PUT_LINE('Date of Birth: '||v_date_of_birth);
END;


DECLARE
v_first_name VARCHAR2(20);
v_last_name VARCHAR2(20);
BEGIN
BEGIN
v_first_name:= 'Carmen';
v_last_name:= 'Miranda';
DBMS_OUTPUT.PUT_LINE (v_first_name|| ' '|| v_last_name);
END;
DBMS_OUTPUT.PUT_LINE (v_first_name|| ' '|| v_last_name);
END;


<<fuera>>
DECLARE
v_father_name VARCHAR2(20):='Patrick';
v_date_of_birth DATE:='20-Abril-1972';
BEGIN
DECLARE
v_child_name VARCHAR2(20):='Mike';
v_date_of_birth DATE:='12-Diciembre-2002';
BEGIN
DBMS_OUTPUT.PUT_LINE('Father''sName: ' || v_father_name);
DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || fuera.v_date_of_birth);
DBMS_OUTPUT.PUT_LINE('Child''sName: ' || v_child_name);
DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || v_date_of_birth);
END;
END;