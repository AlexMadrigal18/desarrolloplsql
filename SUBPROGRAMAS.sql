--SUBPROGRAMAS(PROCEDURES)
--PROCEDURE
--FUNCTION
--PACKAGE
--TRIGGER
--------------------------------------------------------------------------------
--PROCEDURE

CREATE OR REPLACE PROCEDURE add_dept IS
v_dept_id departments.department_id%TYPE;
v_dept_name departments.department_name%TYPE;
BEGIN
v_dept_id:= 290;
v_dept_name:= 'ST-Curriculum';
INSERT INTO departments (department_id, department_name)
VALUES(v_dept_id, v_dept_name);
DBMS_OUTPUT.PUT_LINE('Inserted '|| SQL%ROWCOUNT || ' row.');
END;

BEGIN
add_dept;
END;
SELECT department_id, department_name FROM departments WHERE department_id=290;

--------------------------------------------------------------------------------
--PROCEDURE CON PARAMETROS: LOS PARAMETROS EN EL PROGRAMA SON TOMADOS COMO CONSTANTES

CREATE OR REPLACE PROCEDURE raise_salary (p_id IN copy_employees.employee_id%TYPE, p_percent IN NUMBER)
IS
BEGIN
UPDATE
copy_employees
SET salary = salary * (1 + p_percent/100)
WHERE employee_id = p_id;
END raise_salary;

BEGIN
raise_salary (176, 10);
END;

CREATE OR REPLACE PROCEDURE process_employees
IS
CURSOR emp_cursor IS
SELECT employee_id
FROM
copy_employees;
BEGIN
FOR
v_emp_rec IN emp_cursor
LOOP
raise_salary(v_emp_rec.employee_id, 10);
END LOOP;
END process_employees;

BEGIN
process_employees;
END;
