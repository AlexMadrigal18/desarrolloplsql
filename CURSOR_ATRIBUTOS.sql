--CURSORES, CICLO FOR, REALIZADO EN USUARIO HR

--------------------------------------------------------------------------------
--CON  "FOR" SE REDUCE CODIGO, FETCH SE REALIZA DE FORMA AUTOMATICA
DECLARE
CURSOR cur_emps IS
SELECT employee_id, last_name FROM employees
WHERE department_id = 50;
BEGIN
FOR v_emp_record IN cur_emps LOOP
DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' '|| v_emp_record.last_name);
END LOOP;
END;

--------------------------------------------------------------------------------
--FOR LOOP CON SUBQUERY
BEGIN
FOR
v_emp_record IN (SELECT employee_id, last_name FROM employees 
WHERE department_id= 50)
LOOP
DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id|| ' '||v_emp_record.last_name);
END LOOP;
END;
--------------------------------------------------------------------------------
--CURSOR CON PARAMETROS

DECLARE
CURSOR cur_country (p_region_id NUMBER) IS
SELECT country_id, country_name
FROM countries
WHERE region_id = p_region_id;
v_country_record cur_country%ROWTYPE;
BEGIN
OPEN cur_country (1);
LOOP
FETCH cur_country 
INTO v_country_record;
EXIT WHEN cur_country%NOTFOUND;
DBMS_OUTPUT.PUT_LINE(v_country_record.country_id||' '||v_country_record.country_name);
END LOOP;
CLOSE cur_country;
END;

--------------------------------------------------------------------------------
--PARAMETROS EN CICLO FOR

DECLARE
    CURSOR cur_countries (p_region_id NUMBER, p_country_id CHAR) IS
    SELECT  country_id, country_name
    FROM countries
    WHERE region_id = p_region_id OR country_id = 'BR';
  
BEGIN
    FOR v_country_record IN cur_countries(145,'BR')LOOP
        DBMS_OUTPUT.PUT_LINE(v_country_record.country_id||'----'||v_country_record. country_name);
    END LOOP;
END;

--------------------------------------------------------------------------------
--CUROSRES FOR UPDATE
--FOR UPDATE: BLOQUEA UNA TABLA, WAIT PARA ESPERAR A QUE SE DESBLOQUEE SI HABIA SIDO BLOQUEADA
--NOWAIT PARA NO ESPERAR E IR DIRECTO A LA EXCEPCION 

DECLARE
CURSOR cur_emps IS
SELECT employee_id, salary 
FROM copy_employees
WHERE salary <=20000 FOR UPDATE NOWAIT;  
v_emp_reccur_emps%ROWTYPE;
BEGIN
OPEN cur_emps;
LOOP
FETCH cur_emps INTO v_emp_rec;
EXIT WHEN cur_emps%NOTFOUND;
UPDATE copy_employees
SET salary = v_emp_rec.salary*1.1
WHERE CURRENT OF cur_emps; ---------WHERE CURRENT OF: CONDICIONANTE DEL CURSOR PARA REFERERIRSE A LA FILA ACTUAL Y USAR EL UPDATE
END LOOP;
CLOSE cur_emps;
END;


--------------------------------------------------------------------------------
--CURSOR: UPDATE CON JOIN
DECLARE
CURSOR cur_eds IS
SELECT employee_id, salary, department_name
FROM
my_employeese, my_departments d
WHERE
e.department_id = d.department_id
FOR UPDATE OF salary NOWAIT; ---------SE ESPECIFICA UNA COLUMNA CUALQUIERA DE LA TABLA QUE SE QUIERE BLOQUEAR
BEGIN
FOR
v_eds_rec IN cur_eds
LOOP
UPDATE
my_employees
SET salary = v_eds_rec.salary* 1.1
WHERE CURRENT OF cur_eds;
END LOOP;
END;

--------------------------------------------------------------------------------
--CURSORES MULTIPLES: SE PUEDEN UTILIZAR VARIOS CUROSRES EN UN SOLO BLOQUE PLSQL
DECLARE
CURSOR cur_loc IS SELECT * FROM locations;
CURSOR cur_dept (p_locid NUMBER) IS
SELECT * FROM departments WHERE location_id = p_locid;
v_locrec cur_loc%ROWTYPE;
v_deptrec cur_dept%ROWTYPE;
BEGIN
OPEN cur_loc;
LOOP
FETCH cur_loc INTO v_locrec;
EXIT WHEN cur_loc%NOTFOUND;
DBMS_OUTPUT.PUT_LINE(v_locrec.city);
OPEN cur_dept (v_locrec.location_id);
LOOP
FETCH cur_dept INTO v_deptrec;
EXIT WHEN cur_dept%NOTFOUND;
DBMS_OUTPUT.PUT_LINE(v_deptrec.department_name);
END LOOP;
CLOSE cur_dept;
END LOOP;
CLOSE cur_loc;
END;

--------------------------------------------------------------------------------
--Y SE PUEDEN UTILIZAR VARIOS PROCEDIMIENTOS COMO UN FOR O FOR UPDATE CON MULTIPLES CURSORES
DECLARE
CURSOR cur_dept IS SELECT * FROM my_departments;
CURSOR cur_emp (p_dept_id NUMBER) IS
SELECT * FROM my_employees WHERE department_id = p_dept_id
FOR UPDATE NOWAIT;
BEGIN
FOR v_deptrec IN cur_dept  
LOOP
DBMS_OUTPUT.PUT_LINE(v_deptrec.department_name);
FOR v_emprec IN cur_emp (v_deptrec.department_id) 
LOOP
DBMS_OUTPUT.PUT_LINE(v_emprec.last_name);
IF v_deptrec.location_id = 1700 AND v_emprec.salary < 10000
THEN UPDATE
my_employees SET salary = salary * 1.1
WHERE CURRENT OF cur_emp;
END IF;
END LOOP;
END LOOP;
END;
